import React from "react";
import "./styles.css";
import Select, { components } from "react-select";

interface Option {
  label: string;
  value: string;
  checked: boolean;
}
const options = [
  {
    value: "option1",
    label: "option1",
    checked: true
  },
  {
    value: "option2",
    label: "option2",
    checked: false
  }
];

//interface Cust extends OptionTypeBase {}
const CheckboxOPtion: React.FC<components.MultiValueProps<Option>> = (
  props
) => {
  console.log("props", props);
  return (
    <div {...props.innerProps} style={{ backgroundColor: "red" }}>
      <input type="checkbox" checked={props.data.checked} onChange={() => {}} />
      <label htmlFor="">{props.data.label}</label>
    </div>
  );
};

const renderOption = (option: any) => {
  console.log(option);
  return (
    <div style={{ backgroundColor: "red" }}>
      <input type="checkbox" checked={option.data.checked} />
      <label htmlFor="">
        {option.label} {option.checked}
      </label>
    </div>
  );
};
export default function App() {
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      <Select
        isMulti
        options={options}
        components={{
          MultiValue: CheckboxOPtion,
          Option: renderOption
        }}
      />
    </div>
  );
}
